import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TxHandler {
	private UTXOPool pool;

	/* Creates a public ledger whose current UTXOPool (collection of unspent 
	 * transaction outputs) is utxoPool. This should make a defensive copy of 
	 * utxoPool by using the UTXOPool(UTXOPool uPool) constructor.
	 */
	public TxHandler(UTXOPool utxoPool) {
		pool = new UTXOPool(utxoPool);
	}

	/* Returns true if 
	 * (1) all outputs claimed by tx are in the current UTXO pool, 
	 * (2) the signatures on each input of tx are valid, 
	 * (3) no UTXO is claimed multiple times by tx, 
	 * (4) all of tx’s output values are non-negative, and
	 * (5) the sum of tx’s input values is greater than or equal to the sum of   
	        its output values;
	   and false otherwise.
	 */

	public boolean isValidTx(Transaction tx) {
		// (1)
		if (doTxInputsContainClaimedOutputNotInPool(tx.getInputs().stream()))
			return false;
		// (2)
		for (int i = 0; i < tx.numInputs(); i++) {
			if (!isSignatureOfTxInputValid(tx, i))
				return false;
		}
		// (3)
		Set<UTXO> spendUTXOs = new HashSet<>();
		for (Transaction.Input input : tx.getInputs()) {
			if (!spendUTXOs.add(inputToUTXO(input)))
				return false;
		}
		// (4)
		if (tx.getOutputs().stream().anyMatch(output -> output.value < 0))
			return false;
		// (5)
		double sumOfInput = tx.getInputs().stream().mapToDouble(input -> inputToLinkedOutput(input).value).sum();
		double sumOfOutput = tx.getOutputs().stream().mapToDouble(output -> output.value).sum();
		return sumOfInput >= sumOfOutput;
	}

	private boolean doTxInputsContainClaimedOutputNotInPool(Stream<Transaction.Input> inputs) {
		return inputs.map(this::inputToUTXO).anyMatch(utxo -> !pool.contains(utxo));
	}

	/* Handles each epoch by receiving an unordered array of proposed 
	 * transactions, checking each transaction for correctness, 
	 * returning a mutually valid array of accepted transactions, 
	 * and updating the current UTXO pool as appropriate.
	 */
	public Transaction[] handleTxs(Transaction[] possibleTxs) {
		ArrayList<Transaction> unhandledTxs = new ArrayList<>(Arrays.asList(possibleTxs));
		List<Transaction> handledTxs = new ArrayList<>(possibleTxs.length);
		List<Transaction> executedTxs;
		do {
			executedTxs = passThroughTransactionsAndExecuteValid(unhandledTxs);
			handledTxs.addAll(executedTxs);
			unhandledTxs.removeAll(executedTxs);
		} while (!executedTxs.isEmpty()); // Continue trying to execute transactions until none are executed anymore
		return handledTxs.toArray(Transaction[]::new);
	}

	private UTXO inputToUTXO(Transaction.Input input) {
		return new UTXO(input.prevTxHash, input.outputIndex);
	}

	private Transaction.Output inputToLinkedOutput(Transaction.Input input) {
		return pool.getTxOutput(inputToUTXO(input));
	}

	private boolean isSignatureOfTxInputValid(Transaction tx, int index) {
		Transaction.Input input = tx.getInput(index);
		byte[] rawSignatureData = tx.getRawDataToSign(index);
		UTXO inputUtxo = inputToUTXO(input);
		// Return false if output can't be found
		if (!pool.contains(inputUtxo)) return false;
		RSAKey ownerPkKey = pool.getTxOutput(inputUtxo).address;

		return ownerPkKey.verifySignature(rawSignatureData, input.signature);
	}

	private List<Transaction> passThroughTransactionsAndExecuteValid(List<Transaction> unprocessedTxs) {
		List<Transaction> processedTxs = new ArrayList<>();
		for (Transaction transaction : unprocessedTxs) {
			try {
				executeTransaction(transaction);
				processedTxs.add(transaction);
			} catch (IllegalArgumentException e) {
				// Transaction might be valid later, just don't execute it right now
			}
		}
		return Collections.unmodifiableList(processedTxs);
	}

	private void executeTransaction(Transaction transaction) {
		if (!isValidTx(transaction))
			throw new IllegalArgumentException("Transaction must be valid in order to be executed!");
		executeValidTransaction(transaction);
	}

	private void executeValidTransaction(Transaction transaction) {
		// Spend Tx inputs
		transaction.getInputs().stream().map(this::inputToUTXO).forEach(pool::removeUTXO);
		// Add Tx outputs to UTXO pool
		IntStream.range(0, transaction.numOutputs())
				.forEach(index -> pool.addUTXO(
						new UTXO(transaction.getHash(), index),
						transaction.getOutput(index))
				);
	}

} 
